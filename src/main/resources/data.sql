INSERT INTO estado (id, nome, sigla) VALUES (1, 'Goiás', 'GO');
INSERT INTO estado (id, nome, sigla) VALUES (2, 'Mato Grosso', 'MT');
INSERT INTO estado (id, nome, sigla) VALUES (3, 'Mato Grosso do Sul', 'MS');

-- ALTER SEQUENCE estado_id_seq RESTART WITH 4;

INSERT INTO cidade (id, nome, capital, estado_id) VALUES (1, 'Goiânia', true, 1);
INSERT INTO cidade (id, nome, capital, estado_id) VALUES (2, 'Anápolis', false, 1);
INSERT INTO cidade (id, nome, capital, estado_id) VALUES (3, 'Cuiabá', true, 2);
INSERT INTO cidade (id, nome, capital, estado_id) VALUES (4, 'Barra do Garças', false, 2);
INSERT INTO cidade (id, nome, capital, estado_id) VALUES (5, 'Campo Grande', true, 3);
INSERT INTO cidade (id, nome, capital, estado_id) VALUES (6, 'Dourados', false, 3);

-- ALTER SEQUENCE cidade_seq RESTART WITH 4;

INSERT INTO contato (id, nome, endereco, telefone, cidade_id)
VALUES (1, 'Maria das Dores', 'Rua das Graças, Jardim das Flores', '11 1111-1111', 1);
INSERT INTO contato (id, nome, endereco, telefone, cidade_id)
VALUES (2, 'Carla Torres', 'Av. 1, Centro', '22 2222-2222', 1);
INSERT INTO contato (id, nome, endereco, telefone, cidade_id)
VALUES (3, 'Ana Martins', 'Av. das Luzes, Bairro Nossa Senhora', '33 3333-3333', 2);
INSERT INTO contato (id, nome, endereco, telefone, cidade_id)
VALUES (4, 'José da Silva', 'Rua Mangueiras, Jardim Esperança', '44 4444-4444', 2);
INSERT INTO contato (id, nome, endereco, telefone, cidade_id)
VALUES (5, 'Pedro Ferreira', 'Rua das Graças, Jardim das Flores', '55 5555-5555', 2);

-- ALTER SEQUENCE contato_seq RESTART WITH 6;
