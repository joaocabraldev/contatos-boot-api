package br.com.joaocabral.contatosbootng.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.contatosbootng.model.Contato;
import br.com.joaocabral.contatosbootng.repository.ContatoRepository;
import javassist.NotFoundException;

@RestController
@RequestMapping("/api/contatos")
@CrossOrigin("*")
public class ContatosController {
    
    @Autowired
    private ContatoRepository repository;

    @GetMapping
    public List<Contato> index() {
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public Contato find(@PathVariable Long id) throws NotFoundException {
        return repository.findById(id)
            .orElseThrow(() -> new NotFoundException("Contato não encontrado!"));
    }

    @PostMapping
    public Contato save(@RequestBody Contato contato) {
        return repository.save(contato);
    }

    @PutMapping("/{id}")
    public Contato update(@RequestBody Contato novoContato, @PathVariable Long id) {
        Contato contato = repository.findById(id).get();
        contato.setNome(novoContato.getNome());
        contato.setEndereco(novoContato.getEndereco());
        contato.setTelefone(novoContato.getTelefone());
        contato.setCidade(novoContato.getCidade());
        return repository.save(contato);
    }

    @DeleteMapping
    public void delete(@PathVariable Long id) {
        repository.deleteById(id);
    }

}