package br.com.joaocabral.contatosbootng.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.contatosbootng.model.Cidade;
import br.com.joaocabral.contatosbootng.repository.CidadeRepository;
import javassist.NotFoundException;

@RestController
@RequestMapping("/api/cidades")
@CrossOrigin("*")
public class CidadesController {
    
    @Autowired
    private CidadeRepository repository;

    @GetMapping
    public List<Cidade> index() {
        return repository.findAll();
    }

    @GetMapping("/estado/{id}")
    public List<Cidade> findByEstadoId(@PathVariable Long id) {
        return repository.findByEstadoId(id);
    }

    @GetMapping("/{id}")
    public Cidade find(@PathVariable Long id) throws NotFoundException {
        return repository.findById(id)
            .orElseThrow(() -> new NotFoundException("Cidade não encontrada!"));
    }

    @PostMapping
    public Cidade save(@RequestBody Cidade cidade) {
        return repository.save(cidade);
    }

    @PutMapping("/{id}")
    public Cidade update(@RequestBody Cidade novaCidade, @PathVariable Long id) {
        Cidade cidade = repository.findById(id).get();
        cidade.setNome(novaCidade.getNome());
        cidade.setCapital(novaCidade.getCapital());
        cidade.setEstado(novaCidade.getEstado());
        return repository.save(cidade);
    }

    @DeleteMapping
    public void delete(@PathVariable Long id) {
        repository.deleteById(id);
    }

}