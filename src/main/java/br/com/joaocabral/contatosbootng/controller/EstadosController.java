package br.com.joaocabral.contatosbootng.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaocabral.contatosbootng.model.Estado;
import br.com.joaocabral.contatosbootng.repository.EstadoRepository;
import br.com.joaocabral.contatosbootng.service.EstadosRelatorioService;
import javassist.NotFoundException;
import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/api/estados")
@CrossOrigin("*")
public class EstadosController {
    
    @Autowired
    private EstadoRepository repository;
    
    @Autowired
    private EstadosRelatorioService relatorioService;

    @GetMapping
    public List<Estado> index() {
        return repository.findAll();
    }

    @GetMapping("/relatorio")
    public void report(HttpServletResponse response) throws JRException, IOException {
    	response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline; filename=estados.pdf");
		final OutputStream outputStream = response.getOutputStream();
    	relatorioService.gerarRelatorio(outputStream);
    }

    @GetMapping("/{id}")
    public Estado find(@PathVariable Long id) throws NotFoundException {
        return repository.findById(id)
            .orElseThrow(() -> new NotFoundException("Estado não encontrado!"));
    }
    

    @PostMapping
    public Estado save(@RequestBody Estado estado) {
        return repository.save(estado);
    }

    @PutMapping("/{id}")
    public Estado update(@RequestBody Estado novoEstado, @PathVariable Long id) {
        Estado estado = repository.findById(id).get();
        estado.setNome(novoEstado.getNome());
        estado.setSigla(novoEstado.getSigla());
        return repository.save(estado);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        repository.deleteById(id);
    }

}