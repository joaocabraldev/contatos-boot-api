package br.com.joaocabral.contatosbootng.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.joaocabral.contatosbootng.model.Estado;
import br.com.joaocabral.contatosbootng.repository.EstadoRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class EstadosRelatorioService {

	@Autowired
	private EstadoRepository repository;
	
	public void gerarRelatorio(OutputStream outputStream) throws JRException, IOException {
		List<Estado> estados = repository.findAll();
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(estados, false);
		
		Map<String, Object> parametros = new HashMap<>();

		InputStream relatorioStream = getClass().getResourceAsStream("/estados.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(relatorioStream);
		JasperPrint print = JasperFillManager.fillReport(jasperReport, parametros, dataSource);
		
		JasperExportManager.exportReportToPdfStream(print, outputStream);
	}
	
}
