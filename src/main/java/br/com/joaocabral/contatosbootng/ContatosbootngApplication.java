package br.com.joaocabral.contatosbootng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContatosbootngApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContatosbootngApplication.class, args);
	}

}
