package br.com.joaocabral.contatosbootng.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.joaocabral.contatosbootng.model.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Long> {

}
