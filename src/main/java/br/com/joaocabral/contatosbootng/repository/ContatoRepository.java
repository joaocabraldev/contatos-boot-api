package br.com.joaocabral.contatosbootng.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.joaocabral.contatosbootng.model.Contato;

public interface ContatoRepository extends JpaRepository<Contato, Long> {

}